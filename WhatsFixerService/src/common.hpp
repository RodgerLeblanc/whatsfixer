/*
 * common.hpp
 *
 *  Created on: 2016-05-29
 *      Author: Roger
 */

#ifndef COMMON_HPP_
#define COMMON_HPP_

#include <stdlib.h>


/*** ▼ ▼ ▼ CHANGE VALUES BETWEEN COMMENTS FOR EVERY NEW APP ▼ ▼ ▼ ***/
#define PACKAGE_NAME                                QString("com.CellNinja.WhatsFixer")
#define MOMENTICS_PROJECT_NAME                      QString("WhatsFixer")
#define BUG_REPORT_EMAIL_TO                         QString("support@yourCompany.com")
#define SHOULD_ALWAYS_KEEP_HEADLESS_ALIVE           false
#define HEADLESS_COMMUNICATION_PORT_UI              17488
#define HEADLESS_COMMUNICATION_PORT_HL              HEADLESS_COMMUNICATION_PORT_UI + 1
/*** ▲ ▲ ▲ CHANGE VALUES BETWEEN COMMENTS FOR EVERY NEW APP ▲ ▲ ▲ ***/





// App description
#define APP_NAME                                    QCoreApplication::applicationName()
#define APP_VERSION                                 QCoreApplication::applicationVersion()

// Invocation targets and actions
#define UI_INVOCATION_TARGET                        PACKAGE_NAME
#define HEADLESS_INVOCATION_TARGET                  PACKAGE_NAME + QString("Service")
#define HEADLESS_INVOCATION_START_ACTION            HEADLESS_INVOCATION_TARGET + QString(".START")
#define HEADLESS_INVOCATION_SHUTDOWN_ACTION         HEADLESS_INVOCATION_TARGET + QString(".SHUTDOWN")
#define HEADLESS_INVOCATION_SEND_BUG_REPORT_ACTION  HEADLESS_INVOCATION_TARGET + QString(".SEND_BUG_REPORT")
#define HEADLESS_INVOCATION_SEND_LOG_TO_HUB_ACTION  HEADLESS_INVOCATION_TARGET + QString(".SEND_LOG_TO_HUB")
#define HEADLESS_INVOCATION_RESET_WATCH_ACTION      HEADLESS_INVOCATION_TARGET + QString("RESET_WATCH")

// Path to files
#define LOG_FILE                                    "data/log.txt"
#define SETTINGS_FILE                               "data/settings.json"
#define LOG_SHAREWITH_PATH                          "sharewith/pim/log.txt"
#define CHANGELOG_FILE                              "app/native/assets/Changelog/changelog.json"

// Read and write default retry values
#define READ_FILE_MAX_RETRY                         30
#define WRITE_FILE_MAX_RETRY                        30
#define READ_FILE_ERROR_SLEEP_TIME_MS               1000 * 100
#define WRITE_FILE_ERROR_SLEEP_TIME_MS              1000 * 100

// Those are the keys for the map passed in HeadlessCommunication protocol
#define HEADLESS_COMMUNICATION_REASON               QString("HEADLESS_COMMUNICATION_REASON")
#define HEADLESS_COMMUNICATION_DATA                 QString("HEADLESS_COMMUNICATION_DATA")

// Reasons used with HeadlessCommunication
#define LOG_READY_FOR_BUG_REPORT                    QString("LOG_READY_FOR_BUG_REPORT")
#define LOGGER_LOG_THIS                             QString("LOGGER_LOG_THIS")
#define SETTINGS_CLEAR                              QString("SETTINGS_CLEAR")
#define SETTINGS_REMOVE_KEY                         QString("SETTINGS_REMOVE_KEY")
#define SETTINGS_SAVE_THIS                          QString("SETTINGS_SAVE_THIS")
#define SETTINGS_FILE_UPDATED                       QString("SETTINGS_FILE_UPDATED")
#define SETTINGS_INSTALLED_DATE                     QString("INSTALLED_DATE")
#define SETTINGS_LAST_VERSION_LOADED                QString("LAST_VERSION_LOADED")
#define SETTINGS_DEBUG_MODE_ACTIVE                  QString("DEBUG_MODE_ACTIVE")
#define SETTINGS_CLEAR_CACHE_AUTOMATICALLY          QString("CLEAR_CACHE_AUTOMATICALLY")
#define SETTINGS_MAX_CACHE_SIZE                     QString("MAX_CACHE_SIZE")

// Default toString() format for DateTime and Time
#define DATETIME_TO_STRING                          "yyyy/MM/dd hh:mm:ss"
#define TIME_TO_STRING                              "hh:mm:ss"

// Used to differentiate in what environment the code is running
namespace Environment {
    enum Type { Unknown, UI, Headless };
}

#define PERIMETER_HOME                              QString(getenv("PERIMETER_HOME"))
#define WHATSAPP_ORIGINAL_FOLDER                    PERIMETER_HOME + "/shared/misc/android/WhatsApp"
#define WHATSAPP_RELOCATED_FOLDER_PARENT            PERIMETER_HOME + "/removable/sdcard/Android"
#define WHATSAPP_RELOCATED_FOLDER                   WHATSAPP_RELOCATED_FOLDER_PARENT + "/WhatsApp"
#define WHATSAPP_RELOCATED_MEDIA_FOLDER             WHATSAPP_RELOCATED_FOLDER + "/Media"

#define DEFAULT_MAX_CACHE_SIZE                      1000000

#endif /* COMMON_HPP_ */
