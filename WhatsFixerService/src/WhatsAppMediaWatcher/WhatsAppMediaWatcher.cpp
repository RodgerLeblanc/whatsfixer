/*
 * WhatsAppMediaWatcher.cpp
 *
 *  Created on: Sep 17, 2017
 *      Author: roger
 */

#include <WhatsAppMediaWatcher/WhatsAppMediaWatcher.h>

#include <QDir>
#include <QFileInfo>

WhatsAppMediaWatcher::WhatsAppMediaWatcher(Settings* settings, QObject* parent) :
    QObject(parent),
    fileSystemWatcher(new QFileSystemWatcher(this))
{
    this->settings = settings;

    this->resetWatch();

    connect(fileSystemWatcher, SIGNAL(directoryChanged(const QString&)), this, SLOT(onDirectoryChanged(const QString&)));
}

void WhatsAppMediaWatcher::onDirectoryChanged(const QString& path) {
    Logger::log("Service::onDirectoryChanged(): ", path);

    if (QString::compare(path, WHATSAPP_RELOCATED_MEDIA_FOLDER, Qt::CaseInsensitive) == 0) {
        this->resetWatch();
    }
    else {
        /*
        bool isFromImageFolder = path.contains("WhatsApp Images/Sent");
        bool isFromVideoFolder = path.contains("WhatsApp Video/Sent");

        if (isFromImageFolder || isFromVideoFolder) {
            QDir dir(path);
            QFileInfoList fileInfoList = dir.entryInfoList(QDir::Files, QDir::Time);
            if (!fileInfoList.isEmpty()) {
                QFileInfo fileInfo = fileInfoList.first();
                bool isGif = QString::compare(fileInfo.suffix(), "gif", Qt::CaseInsensitive) == 0;
                if ((isFromImageFolder && isGif) || isFromVideoFolder) {
                    QString absoluteFilePath = fileInfo.absoluteFilePath();
                    if (absoluteFilePath.contains("WAA_COPY")) { return; }
                    if (absoluteFilePath.contains("qt_temp")) { return; }

                    if (!listOfFilesCatched.contains(absoluteFilePath)) {
                        Logger::log("Rename this file: " + absoluteFilePath);
                        listOfFilesCatched.append(absoluteFilePath);
                        while (listOfFilesCatched.size() > 20) { listOfFilesCatched.removeFirst(); }

                        bool isVideo = absoluteFilePath.contains("/WhatsApp Video/Sent");
                        WorkerThread* workerThread = new WorkerThread(absoluteFilePath, isVideo, this);
                        connect(workerThread, SIGNAL(renameDone()), this, SLOT(onRenameDone()));
                        workerThread->start();
                    }
                    else {
                        Logger::log("File already renamed in the past: " + absoluteFilePath);
                    }
                }
            }
        }
        */
    }
}

void WhatsAppMediaWatcher::onRenameDone() {
//    listOfFilesCatched.clear();
}

void WhatsAppMediaWatcher::resetWatch() {
    fileSystemWatcher->removePaths(fileSystemWatcher->directories());
    fileSystemWatcher->removePaths(fileSystemWatcher->files());

    QStringList folderPartialNames = QStringList() << "Images" << "Audio" << "Documents" << "Video";

    foreach(QString partialName, folderPartialNames) {
        QString path = WHATSAPP_RELOCATED_MEDIA_FOLDER + QString("/WhatsApp %1/Sent").arg(partialName);
        QDir dir;
        if (!dir.exists(path)) {
            if (!dir.mkpath(path)) {
                LOG(path, "path couldn't be created");
            }
        }
        fileSystemWatcher->addPath(path);
    }

    QString path = WHATSAPP_RELOCATED_MEDIA_FOLDER + QString("/WhatsApp Audio");
    fileSystemWatcher->addPath(path);

    fileSystemWatcher->addPath(WHATSAPP_RELOCATED_MEDIA_FOLDER);
}
