/*
 * WhatsAppMediaWatcher.h
 *
 *  Created on: Sep 17, 2017
 *      Author: roger
 */

#ifndef WHATSAPPMEDIAWATCHER_H_
#define WHATSAPPMEDIAWATCHER_H_

#include "common.hpp"
#include <src/Logger/Logger.h>
#include <src/Settings/Settings.h>
#include <src/WhatsAppMediaWatcher/WorkerThread/WorkerThread.h>

#include <QObject>
#include <QFileSystemWatcher>
#include <QStringList>

class WhatsAppMediaWatcher : public QObject
{
    Q_OBJECT
public:
    WhatsAppMediaWatcher(Settings* settings, QObject* parent);

    void resetWatch();

private slots:
    void onDirectoryChanged(const QString&);
    void onRenameDone();

private:
    QFileSystemWatcher* fileSystemWatcher;
    Settings* settings;

    QStringList listOfFilesCatched;
};

#endif /* WHATSAPPMEDIAWATCHER_H_ */
