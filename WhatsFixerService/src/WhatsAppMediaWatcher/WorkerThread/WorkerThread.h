/*
 * WorkerThread.h
 *
 *  Created on: 2016-07-08
 *      Author: Roger
 */

#ifndef WORKERTHREAD_H_
#define WORKERTHREAD_H_

#include <QObject>
#include <QProcess>
#include <QThread>

class WorkerThread : public QThread
{
    Q_OBJECT

    enum NotificationType {
        NOTIFICATION_CLICK_RETRY,
        NOTIFICATION_RESTART_WHATSAPP
    };

public:
    WorkerThread(const QString& filePath, const bool& isVideo = false, QObject* parent = NULL);

    void run(); // Only function that will run in separate thread

private slots:
    void onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onRenameDone();

private:
    void showNotification(NotificationType type);
    void startProcess(QString command, QStringList arguments);

    QString filePath;
    int maxAttempts;

signals:
    void renameDone();
};

#endif /* WORKERTHREAD_H_ */
