/*
 * WorkerThread.cpp
 *
 *  Created on: 2016-07-08
 *      Author: Roger
 */

#include "WorkerThread.h"
#include <src/Logger/Logger.h>

#include <bb/platform/Notification>
#include <QFileInfo>
#include <QDebug>
#include <QTimer>

using namespace bb::platform;

WorkerThread::WorkerThread(const QString& filePath, const bool& isVideo, QObject* parent) :
    QThread(parent),
    filePath(filePath),
    maxAttempts(isVideo ? 1 : 100)
{
    connect(this, SIGNAL(renameDone()), this, SLOT(onRenameDone()));
    QTimer::singleShot(10 * 60000, this, SLOT(deleteLater())); // Lives a maximum of 10 minutes
}

void WorkerThread::onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    Q_UNUSED(exitCode);

    QProcess* process = static_cast<QProcess*>(sender());

    QByteArray se = (exitStatus == QProcess::NormalExit) ? process->readAllStandardError() : "Command crashed or stopped after timeout";
    QByteArray so = process->readAllStandardOutput();
    QByteArray all = process->readAll();

    process->deleteLater();

    qDebug() << exitCode << se << so << all;
}

void WorkerThread::onRenameDone() {
}

void WorkerThread::run() {
    if (filePath.contains(".opus")) { usleep(2000 * 1000); } // 2 secs

    int attempts = -1;
    QString parentFolder = filePath.left(filePath.lastIndexOf("/") + 1);

    QString fileName = QString(filePath).remove(parentFolder);
    QString fileCopy = parentFolder + "WAA_COPY_" + fileName;

    LOG_VAR(filePath);
    LOG_VAR(fileCopy);

    bool worked = QFile::rename(filePath, fileCopy);
    if (!worked) { return; }
    qDebug() << "Renamed original to _WAA_COPY...";

    QString command = "ls";
    QStringList arguments = QStringList() << "-l" << fileCopy;
    this->startProcess(command, arguments);

    while (++attempts < maxAttempts) {
        worked = QFile::copy(fileCopy, filePath);
        qDebug() << "attempts:" << attempts;
        if (!worked) { usleep(50 * 1000); } // 50ms
    }

    if (!worked) { return; }
    qDebug() << "Copied...";

    command = "ls";
    arguments = QStringList() << "-l" << filePath;
    this->startProcess(command, arguments);

    /*
    worked = QFile::remove(fileCopy);
    if (!worked) { return; }
    qDebug() << "Removed...";
    */

    command = "ls";
    arguments = QStringList() << "-l" << fileCopy;
    this->startProcess(command, arguments);

    emit renameDone();
}

void WorkerThread::showNotification(NotificationType type) {
    Notification* notification = new Notification();
    QString iconPath;

    switch (type) {
        case NOTIFICATION_CLICK_RETRY: {
            iconPath = QString::fromUtf8("file://%1/app/native/assets/Images/retry.png").arg(QDir::currentPath());
            notification->setTitle(tr("Click Retry in WhatsApp"));
            notification->setBody(tr("In WhatsApp for Android, click on the Retry icon (↥)."));
            QTimer::singleShot(3000, notification, SLOT(deleteFromInbox()));
            break;
        }
        case NOTIFICATION_RESTART_WHATSAPP: {
            iconPath = QString::fromUtf8("file://%1/app/native/assets/Images/restart.png").arg(QDir::currentPath());
            notification->setTitle(tr("Restart WhatsApp and click on Retry"));
            notification->setBody(tr("Close WhatsApp, restart it and click on the Retry icon (↥)."));
            QTimer::singleShot(60000, notification, SLOT(deleteFromInbox()));
            break;
        }
        default: break;
    }

    notification->setIconUrl(iconPath);
    notification->notify();

    this->deleteLater();
}

void WorkerThread::startProcess(QString command, QStringList arguments) {
    QProcess process;

    process.start(command, arguments);
    process.waitForFinished();
    qDebug() << "Error:" << process.error() << process.errorString();
}
