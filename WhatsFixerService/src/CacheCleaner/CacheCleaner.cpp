/*
 * CacheCleaner.cpp
 *
 *  Created on: Oct 30, 2017
 *      Author: roger
 */

#include "CacheCleaner.h"

#include <QDir>

QStringList mediaFolders = QStringList() << "WhatsApp Voice Notes" << "WhatsApp Animated Gifs" << "WhatsApp Images" << "WhatsApp Documents" << "WhatsApp Video" << "WhatsApp Audio";

CacheCleaner::CacheCleaner(Settings* settings, QObject* parent) :
    QObject(parent),
    deviceActive(new DeviceActive(this))
{
    this->settings = settings;

    connect(deviceActive, SIGNAL(deviceActiveChanged(const bool&)), this, SLOT(onDeviceActiveChanged(const bool&)));
}

void CacheCleaner::getAllWhatsAppFiles(QString path, QMap<int, QFileInfo> &allFilesSorted, int &totalSize) {
    qDebug() << "CacheCleaner::getAllWhatsAppFiles()" << path;

    QDir dir(path);

    QDateTime now = QDateTime::currentDateTime();

    QFileInfoList fileInfoList = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files, QDir::DirsFirst);
    foreach (QFileInfo fileInfo, fileInfoList) {
        if (fileInfo.absoluteFilePath() == path)
            continue;

        if (fileInfo.isDir()) {
            this->getAllWhatsAppFiles(fileInfo.absoluteFilePath(), allFilesSorted, totalSize);
        }
        else {
            int secsSinceModified = fileInfo.lastModified().secsTo(now);
            while (allFilesSorted.contains(secsSinceModified))
                secsSinceModified++;

            allFilesSorted.insert(secsSinceModified, fileInfo);
            totalSize += fileInfo.size() / 1000;
        }
    }
}

void CacheCleaner::onDeviceActiveChanged(const bool& active) {
    if (active)
        return;

    if (!settings->value(SETTINGS_CLEAR_CACHE_AUTOMATICALLY, false).toBool())
        return;

    QDateTime now = QDateTime::currentDateTime();
    if (lastCleanupDate.date().dayOfWeek() == now.date().dayOfWeek())
        return;

    QMap<int, QFileInfo> allFilesSorted;
    int totalSize = 0;

    foreach (QString mediaFolder, mediaFolders)
        this->getAllWhatsAppFiles(WHATSAPP_RELOCATED_MEDIA_FOLDER + "/" + mediaFolder, allFilesSorted, totalSize);

    int maxSize = settings->value(SETTINGS_MAX_CACHE_SIZE, DEFAULT_MAX_CACHE_SIZE).toInt();

    if (totalSize <= maxSize)
        return;

    LOG("WhatsApp folder size is bigger than max cache size, cleanup some files.", totalSize);

    QMap<int, QFileInfo>::iterator i = allFilesSorted.begin();

    QDir dir;
    do {
        QFileInfo fileInfo = i.value();
        int fileSize = fileInfo.size() / 1000;

        if (QFile::remove(fileInfo.absoluteFilePath()))
            totalSize -= fileSize;

    } while (totalSize > maxSize && (++i != allFilesSorted.end()));

    lastCleanupDate = now;

    LOG("Finished cleanup, WhatsApp Media files total size:", totalSize);
}
