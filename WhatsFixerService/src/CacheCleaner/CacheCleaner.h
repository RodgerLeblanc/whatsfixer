/*
 * CacheCleaner.h
 *
 *  Created on: Oct 30, 2017
 *      Author: roger
 */

#ifndef CACHECLEANER_H_
#define CACHECLEANER_H_

#include "common.hpp"
#include <src/DeviceActive/DeviceActive.h>
#include <src/Logger/Logger.h>
#include <src/Settings/Settings.h>

#include <QObject>
#include <QFileInfo>

class CacheCleaner : public QObject
{
    Q_OBJECT
public:
    CacheCleaner(Settings* settings, QObject* parent = NULL);

private slots:
    void onDeviceActiveChanged(const bool&);

private:
    void getAllWhatsAppFiles(QString path, QMap<int, QFileInfo> &allFilesSorted, int &totalSize);

    DeviceActive* deviceActive;
    Settings* settings;

    QDateTime lastCleanupDate;
};

#endif /* CACHECLEANER_H_ */
