APP_NAME = WhatsFixerService

CONFIG += qt warn_on

include(config.pri)
INCLUDEPATH += ../../$${APP_NAME}/src

LIBS += -lbb -lbbdata -lbbdevice -lbbplatform -lbbsystem 
QT += sql declarative network
