import bb.cascades 1.4

Container {
    layout: DockLayout {}
    horizontalAlignment: HorizontalAlignment.Fill
    background: Color.White
    leftPadding: ui.du(1)
    rightPadding: leftPadding
    topPadding: leftPadding
    bottomPadding: leftPadding
    
    topMargin: ui.du(3)
    bottomMargin: ui.du(3)
    
    Container {
        layout: DockLayout {}
        horizontalAlignment: HorizontalAlignment.Fill
        background: mainPage.greenColor
        leftPadding: ui.du(1)
        rightPadding: leftPadding
        topPadding: leftPadding
        bottomPadding: leftPadding

        Container {
            horizontalAlignment: HorizontalAlignment.Center
            topMargin: ui.du(3)
            bottomMargin: topMargin
            Label {
                text: qsTr("Maximum folder cache size") + Retranslate.onLocaleOrLanguageChanged
                horizontalAlignment: HorizontalAlignment.Center
                multiline: true
                textStyle.fontSize: FontSize.XLarge
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Justify
            }
            Slider {
                id: maxCacheSizeSlider
                value: settings.value("MAX_CACHE_SIZE", 1000000)
                fromValue: 1000
                toValue: 1000000
                horizontalAlignment: HorizontalAlignment.Center
                onImmediateValueChanged: value = Math.round(immediateValue / 1000) * 1000
                onValueChanged: settings.setValue("MAX_CACHE_SIZE", value)
            }
            Container {
                layout: StackLayout { orientation: LayoutOrientation.LeftToRight }
                horizontalAlignment: HorizontalAlignment.Center
                Label {
                    text: (maxCacheSizeSlider.value / 1000)
                    multiline: true
                    textStyle.color: Color.White
                    textStyle.textAlign: TextAlign.Justify
                }
                Label {
                    text: "MB"
                    multiline: true
                    textStyle.color: Color.White
                    textStyle.textAlign: TextAlign.Justify
                }
            }
        }
    }
}
