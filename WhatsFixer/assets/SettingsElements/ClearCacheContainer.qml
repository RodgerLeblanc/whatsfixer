import bb.cascades 1.4

Container {
    property bool clearCache: settings.value("CLEAR_CACHE_AUTOMATICALLY", false)

    layout: DockLayout {}
    horizontalAlignment: HorizontalAlignment.Fill
    background: Color.White
    leftPadding: ui.du(1)
    rightPadding: leftPadding
    topPadding: leftPadding
    bottomPadding: leftPadding
    
    topMargin: ui.du(3)
    bottomMargin: ui.du(3)
    
    Container {
        layout: DockLayout {}
        horizontalAlignment: HorizontalAlignment.Fill
        background: mainPage.greenColor
        leftPadding: ui.du(1)
        rightPadding: leftPadding
        topPadding: leftPadding
        bottomPadding: leftPadding

        Container {
            horizontalAlignment: HorizontalAlignment.Center
            leftPadding: ui.du(3)
            rightPadding: leftPadding
            topPadding: leftPadding
            bottomPadding: leftPadding
            background: mainPage.greenColor
            Container {
                layout: StackLayout { orientation: LayoutOrientation.LeftToRight }
                horizontalAlignment: HorizontalAlignment.Center
                Label {
                    text: qsTr("Clear cache automatically") + Retranslate.onLocaleOrLanguageChanged
                    verticalAlignment: VerticalAlignment.Center
                    multiline: true
                    textStyle.fontSize: FontSize.XLarge
                    textStyle.color: Color.White
                    textStyle.textAlign: TextAlign.Justify
                }
                ToggleButton {
                    checked: settings.value("CLEAR_CACHE_AUTOMATICALLY", false)
                    onCheckedChanged: {
                        settings.setValue("CLEAR_CACHE_AUTOMATICALLY", checked)
                        clearCache = checked
                    }
                    verticalAlignment: VerticalAlignment.Center
                }
            }
    
            Label {
                text: qsTr("When enabled, your WhatsApp® Media folders will be clean once per day to remove older files and keep your cache to a predefined maximum size.") + Retranslate.onLocaleOrLanguageChanged
                horizontalAlignment: HorizontalAlignment.Center
                multiline: true
                textStyle.fontSize: FontSize.Small
                textStyle.color: Color.White
                textStyle.textAlign: TextAlign.Justify
            }
        }
    }
}
