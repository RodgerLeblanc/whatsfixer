/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.4
import "Sheets"
import "Tutorial"

Page {
    id: mainPage
    
    property bool debugModeActive: settings.value("DEBUG_MODE_ACTIVE")
    property variant greenColor: Color.create("#FF0DC143")
    
    attachedObjects: [
        SettingsSheet { id: settingsSheet }
    ]

    Container {
        layout: DockLayout {}
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill

        Tutorial { id: tutorial }

        Container {
            visible: app.whatsAppFixed
            horizontalAlignment: HorizontalAlignment.Left
            verticalAlignment: VerticalAlignment.Top
            leftPadding: ui.du(3)
            rightPadding: leftPadding
            topPadding: leftPadding
            bottomPadding: leftPadding
            ImageView {
                imageSource: "asset:///Images/fixed.png"
                maxWidth: ui.du(40)
            }
        }
        
        Container {
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Bottom
            leftPadding: ui.du(3)
            rightPadding: leftPadding
            topPadding: leftPadding
            bottomPadding: leftPadding
            gestureHandlers: TapHandler { onTapped: settingsSheet.open() }
            ImageView {
                imageSource: "asset:///Images/settings.png"
                maxWidth: ui.du(10)
            }
        }
        
        ActivityIndicator {
            minHeight: ui.du(10)
            minWidth: minHeight
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            running: app.isFixing
            visible: app.isFixing
        }
        
        Container {
            visible: mainPage.debugModeActive
            verticalAlignment: VerticalAlignment.Top
            horizontalAlignment: HorizontalAlignment.Center
            topPadding: ui.du(3)
            bottomPadding: topPadding
            Button {
                text: "Shutdown"
                horizontalAlignment: HorizontalAlignment.Center
                onClicked: app.shutdown()
            }
        }
    }
}
