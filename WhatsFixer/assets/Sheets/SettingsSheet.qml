import bb.cascades 1.4
import bb.system 1.2
import "../SettingsElements"

Sheet {
    id: settingsSheet
    
    peekEnabled: false

    Page {
        titleBar: TitleBar {
            title: qsTr("Settings") + Retranslate.onLocaleOrLanguageChanged
            acceptAction: ActionItem {
                title: qsTr("OK") + Retranslate.onLocaleOrLanguageChanged
                onTriggered: { settingsSheet.close() }
            }
        }
        
        ScrollView {
            Container {
                layout: DockLayout {}
                background: mainPage.greenColor
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                Container {
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    leftPadding: ui.du(3)
                    rightPadding: leftPadding
                    topPadding: leftPadding
                    bottomPadding: leftPadding
                    
                    ClearCacheContainer { id: clearCache }
                    CacheMaxSizeContainer { enabled: clearCache.clearCache }
                }
            }
        }
    }
}