import bb.cascades 1.3
import bb.system 1.2

Container {
    id: tutorialContainer
        
    layout: DockLayout {}
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    background: mainPage.greenColor
    
    attachedObjects: [
        LayoutUpdateHandler { id: luhContainer }
    ]
    
    ListView {
        id: listView
        
        property int doubleTaps: 0

        property int pageShown: 0
        property int items: 0
        
        property int deviceWidth: luhContainer.layoutFrame.width
        property int deviceHeight: luhContainer.layoutFrame.height
        property int itemLeftPadding: luhContainer.layoutFrame.width * 0.05
        property int itemTopPadding: luhContainer.layoutFrame.height * 0.05
        property variant background: tutorialContainer.background
        
        onItemsChanged: pageIndicator.populatePageIndicator()
        
        scrollRole: ScrollRole.Main
        dataModel: XmlDataModel { 
            source: qsTr("asset:///Tutorial/tutorialModel.xml") + Retranslate.onLanguageChanged
            onItemsChanged: listView.items = childCount(0)
        }
        layout: StackListLayout { orientation: LayoutOrientation.LeftToRight }
        flickMode: FlickMode.SingleItem
               
        function onDoubleTapped() {
            if (++doubleTaps >= 5) {
                settings.setValue("DEBUG_MODE_ACTIVE", !mainPage.debugModeActive)
                mainPage.debugModeActive = !mainPage.debugModeActive
                doubleTaps = 0;
            }
        }
        
        function updatePageShown(page) {
            pageShown = page
        }
        
        listItemComponents: [
            ListItemComponent {
                type: "item"
                Container {
                    id: itemContainer

                    layout: DockLayout {}
                    minHeight: itemContainer.ListItem.view.deviceHeight
                    maxHeight: minHeight
                    minWidth: itemContainer.ListItem.view.deviceWidth
                    maxWidth: minWidth
                    preferredHeight: minHeight
                    preferredWidth: minWidth
                    
                    attachedObjects: [
                        LayoutUpdateHandler {
                            id: luhPosition
                            onLayoutFrameChanged: {
                                if (luhPosition.layoutFrame.x >= -itemContainer.preferredWidth / 2 && luhPosition.layoutFrame.x <= itemContainer.preferredWidth / 2) {
                                    itemContainer.ListItem.view.updatePageShown(ListItemData.page)
                                } 
                            }
                        }                     
                    ]
                                        
                    gestureHandlers: [
                        DoubleTapHandler {
                            onDoubleTapped: {
                                itemContainer.ListItem.view.onDoubleTapped()
                            }
                        }
                    ]
                    
                    ScrollView {
                        Container {
                            layout: DockLayout {}
                            minHeight: itemContainer.ListItem.view.deviceHeight
                            maxHeight: minHeight
                            minWidth: itemContainer.ListItem.view.deviceWidth
                            maxWidth: minWidth
                            preferredHeight: minHeight
                            preferredWidth: minWidth
                            Container {
                                horizontalAlignment: HorizontalAlignment.Center
                                verticalAlignment: VerticalAlignment.Center
                                Container {
                                    bottomPadding: ui.du(3)
                                    Label {
                                        text: ListItemData.description
                                        multiline: true
                                        textStyle.textAlign: TextAlign.Center
                                        textStyle.fontSize: FontSize.XLarge
                                        horizontalAlignment: HorizontalAlignment.Center
                                        textStyle.color: Color.White
                                        content.flags: TextContentFlag.ActiveText
                                    }
                                }
                                Button {
                                    text: ListItemData.buttonText
                                    enabled: !app.whatsAppFixed
                                    horizontalAlignment: HorizontalAlignment.Center
                                    visible: ListItemData.page == 1
                                    onClicked: app.shutdownWhatsApp()
                                }
                                Button {
                                    text: app.whatsAppFixed ? qsTr("Done") : ListItemData.buttonText
                                    enabled: !app.whatsAppFixed && !app.isFixing
                                    horizontalAlignment: HorizontalAlignment.Center
                                    visible: ListItemData.page == 2
                                    onClicked: app.fixIt()
                                }
                                Button {
                                    text: "Remove fix"
                                    enabled: app.whatsAppFixed
                                    visible: app.whatsAppFixed && ListItemData.page == 2
                                    horizontalAlignment: HorizontalAlignment.Center
                                    onClicked: app.unfixIt()
                                }
                            }
                        }
                    }
                }
            }
        ]    
    }
    
    Container {
        id: pageIndicator
        layout: StackLayout { orientation: LayoutOrientation.LeftToRight }
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Bottom
        bottomPadding: ui.du(3)
        
        onCreationCompleted: populatePageIndicator()
        
        function populatePageIndicator() {
            if (pageIndicator.count() > 0)
                pageIndicator.removeAll()
            
            for (var i = 0; i < listView.items; i++) {
                var control = indicator.createObject(pageIndicator)
                control.index = i
                pageIndicator.add(control)
            }
        }
        
        attachedObjects: ComponentDefinition {
            id: indicator
            content: Container {
                property int index

                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                leftPadding: ui.du(1)
                rightPadding: leftPadding
                Container {
                    minHeight: ui.du(3)
                    minWidth: minHeight
                    background: Color.White
                    opacity: index == listView.pageShown ? 1 : 0.5
                    gestureHandlers: TapHandler { onTapped: listView.scrollToItem([index], ScrollAnimation.Smooth) }
                }
            }
        }
    }
}
