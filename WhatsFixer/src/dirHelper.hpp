namespace DirHelper {
    static bool copyDir(QString source, QString destination) {
        QDir dir(source);

        QFileInfoList list = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files | QDir::Hidden, QDir::DirsFirst);
        foreach (QFileInfo fileInfo, list) {
            if (fileInfo.isSymLink()) { return false; }

            QString sourcePath = fileInfo.filePath();
            QString destinationPath = destination + "/" + fileInfo.fileName();

            if (fileInfo.isDir()) {
                if (!dir.mkpath(destinationPath)) { return false; }
                if (!copyDir(sourcePath, destinationPath)) { return false; }
            }
            else if (fileInfo.isFile()) {
                if (!QFile::copy(sourcePath, destinationPath)) { return false; }
            }
        }

        return true;
    }

    static bool removeDir(const QString dirName)
    {
        QDir dir(dirName);

        if (dir.exists(dirName)) {
            Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
                if (info.isDir()) {
                    if (!removeDir(info.absoluteFilePath())) { return false; }
                }
                else {
                    if (!QFile::remove(info.absoluteFilePath())) { return false; }
                }
            }

            if (!dir.rmdir(dirName)) { return false; }
        }

        return true;
    }
}
