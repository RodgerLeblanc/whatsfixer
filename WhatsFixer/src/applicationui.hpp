/*
 * Copyright (c) 2013-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include "common.hpp"
#include <src/AppInstallerWatch/AppInstallerWatch.h>
#include <src/HeadlessCommunication/HeadlessCommunication.h>
#include <src/Logger/Logger.h>
#include <src/Settings/Settings.h>

#include <QObject>

namespace bb {
    namespace cascades {
        class LocaleHandler;
    }
    namespace system {
        class InvokeManager;
    }
}

class QTranslator;

class ApplicationUI: public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool isFixing READ isFixing NOTIFY isFixingChanged);
    Q_PROPERTY(bool whatsAppFixed READ whatsAppFixed NOTIFY whatsAppFixedChanged);
    Q_PROPERTY(bool whatsAppFound READ whatsAppFound NOTIFY whatsAppFoundChanged);

public:
    ApplicationUI();
    virtual ~ApplicationUI() { }

    Q_INVOKABLE void fixIt();
    Q_INVOKABLE void invokeHL(QString action);
    Q_INVOKABLE void requestBugReport();
    Q_INVOKABLE void sendToHL(QString action);
    Q_INVOKABLE void shutdown();
    Q_INVOKABLE void shutdownWhatsApp();
    Q_INVOKABLE void unfixIt();

private slots:
    void checkForChangelog();
    void checkWhatsAppFixed();
    void checkWhatsAppInstalled();
    void onAppInstallerWatchInitDone();
    void onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onReceivedData(QString reason, QVariant data);
    void onRenderFenceReached();
    void onSystemLanguageChanged();
    void onWhatsAppInstallIdFound(const QString);

private:
    //Getters
    bool isFixing() { return _isFixing; }
    bool whatsAppFixed() { return _whatsAppFixed; }
    bool whatsAppFound() { return _whatsAppFound; }

    //Setters
    void setIsFixing(bool newValue);
    void setWhatsAppFixed(bool newValue);
    void setWhatsAppFound(bool newValue);

    void createSubFoldersIfNeeded(QString whatsAppFolder);
    void init();
    void sendBugReport();
    void startProcess(QString command, QStringList arguments);

    AppInstallerWatch* appInstallerWatch;
    HeadlessCommunication* headlessCommunication;
    bb::system::InvokeManager* invokeManager;
    bb::cascades::LocaleHandler* localeHandler;
    Settings* settings;
    QTranslator* translator;

    Environment::Type environment;
    bool _isFixing;
    QString supportEmail;
    QString whatsAppIntallId;
    bool _whatsAppFixed;
    bool _whatsAppFound;

signals:
    void isFixingChanged(bool&);
    void newChangelog(QString);
    void whatsAppFixedChanged(bool&);
    void whatsAppFoundChanged(bool&);
};

#endif /* ApplicationUI_HPP_ */
