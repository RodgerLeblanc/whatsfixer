/*
 * AppInstallerWatch.h
 *
 *  Created on: 2016-02-29
 *      Author: Roger
 */

#ifndef APPINSTALLERWATCH_H_
#define APPINSTALLERWATCH_H_

#include <src/PpsWatch/PpsWatch.h>

#include <QObject>
#include <QThread>
#include <QTimer>

class AppInstallerWatch : public QThread
{
    Q_OBJECT

public:
    AppInstallerWatch();

private slots:
    void onPpsFileReady(const QVariantMap&);

private:
    void init();
    void run() { init(); }

    PpsWatch* m_pps;
    QTimer* m_initDoneTimer;

signals:
    void whatsAppInstallIdFound(const QString);
    void initDone();
};

#endif /* APPINSTALLERWATCH_H_ */
