/*
 * AppInstallerWatch.cpp
 *
 *  Created on: 2016-02-29
 *      Author: Roger
 */

#include <src/AppInstallerWatch/AppInstallerWatch.h>

#include <QDebug>
#include <QStringList>
#include <bb/data/JsonDataAccess>
#include <bb/PpsObject>

AppInstallerWatch::AppInstallerWatch() :
    m_pps(NULL),
    m_initDoneTimer(NULL)
{
}

void AppInstallerWatch::init() {
    m_initDoneTimer = new QTimer();
    m_initDoneTimer->setSingleShot(true);
    m_initDoneTimer->setInterval(200);

    m_pps = new PpsWatch("/pps/system/installer/appdetails/.all");
    connect(m_pps, SIGNAL(ppsFileReady(const QVariantMap&)), this, SLOT(onPpsFileReady(const QVariantMap&)));

    connect(m_initDoneTimer, SIGNAL(timeout()), this, SIGNAL(initDone()));
    connect(m_initDoneTimer, SIGNAL(timeout()), m_pps, SLOT(deleteLater()));
    connect(m_initDoneTimer, SIGNAL(timeout()), m_initDoneTimer, SLOT(deleteLater()));
}

void AppInstallerWatch::onPpsFileReady(const QVariantMap& map) {
    if (m_initDoneTimer->isActive())
        m_initDoneTimer->stop();

    foreach(const QString key, map.keys()) {
        if (key == "_ppsMapTitle")
            continue;

        QString packageName;
        const QVariantMap thisKey = map[key].toMap();

        QStringList dnamePath = thisKey["dnamepath"].toString().split("/");
        if (!dnamePath.isEmpty())
            packageName = dnamePath.last();

        if (packageName.startsWith("com.whatsapp")) {
            emit whatsAppInstallIdFound(packageName);
            break;
        }
    }

    m_initDoneTimer->start();
}
